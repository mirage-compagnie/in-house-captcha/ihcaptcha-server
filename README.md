# IHCaptcha: Self Hosted Captcha Server

**In House Captcha** is a captcha management solution that can be hosted in your own datacenter.
It means to provide an alternative to the famous **ReCaptcha** solution by Google.

We don't prétend (yet?) to provide an unbreakable captcha like Google. The main goal of this project 
is to have a non intrusive captcha solution.
