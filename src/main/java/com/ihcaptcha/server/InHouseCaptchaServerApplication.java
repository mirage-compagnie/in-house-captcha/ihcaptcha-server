package com.ihcaptcha.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InHouseCaptchaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(InHouseCaptchaServerApplication.class, args);
	}

}
